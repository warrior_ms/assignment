# Created by : Mustafa Sadriwala
# Assignment : Web Development basics, Roles and Responsibilities, REST v/s SOAP, JSON v/s XML

In this assignment we were supposed to create a git repository in which we were supposed to write details and explaination of the the above mentioned topics.

My repository includes files for: 

1.   Frontend development.
2.   Backend development.
3.   Databases.
4.   Domain / Hosting service providers.
5.   Cloud Service Providers.
6.   Continous Integration / Continous Development tools.
7.   Various other Tools.
8.   Web development Frameworks.
9.   Technology stacks.
10.  Types of status codes
11.  REST v/s SOAP
12.  JSON v/s XML
13.  Various roles in project development.
14.  MVC and Non-MVC design patterns.